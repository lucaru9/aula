from class_based_auth_views.views import LogoutView
from django.conf.urls import patterns, include, url

from django.contrib import admin
from web.views import PortalView, MyLoginView, PortalTeacherView, CrearExamenTeacher, DetailExamView, PortalStudentView, \
    ExamStudentView

admin.autodiscover()

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       url(r'^$', MyLoginView.as_view(), name="login"),
                       url(r'^portal/$', PortalView.as_view(), name="page_principal"),
                       url(r'^logout/$', LogoutView.as_view(), name="logout"),
                       url(r'^portal/teacher/$', PortalTeacherView.as_view(), name="page_teacher"),
                       url(r'^portal/teacher/create-exam/$', CrearExamenTeacher.as_view(), name="page_teacher_create"),
                       url(r'^portal/teacher/exam/(?P<pk>\d+)/$', DetailExamView.as_view(), name="detail_exam"),
                       url(r'^portal/student/$', PortalStudentView.as_view(), name="page_student"),
                       url(r'^portal/student/exam/(?P<pk>\d+)/$', ExamStudentView.as_view(), name="list_question"),

)
