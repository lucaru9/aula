from django.contrib import admin

# Register your models here.
from web.models import Choice, Question, Exam, Student, Teacher, QuestionStudent, QuestionStudentChoice, StudentExam

admin.site.register(Choice)
admin.site.register(Question)
admin.site.register(Student)
admin.site.register(Exam)
admin.site.register(Teacher)
admin.site.register(QuestionStudent)
admin.site.register(QuestionStudentChoice)
admin.site.register(StudentExam)