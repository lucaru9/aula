from django.contrib.auth.models import User
from django.db import models


class Teacher(models.Model):
    user = models.OneToOneField(User, unique=True, related_name='teacher')
    name = models.CharField(max_length=80)
    code = models.CharField(max_length=10, unique=True)

    def __unicode__(self):
        return self.code


class Student(models.Model):
    user = models.OneToOneField(User, unique=True, related_name='student')
    name = models.CharField(max_length=80)
    code = models.CharField(max_length=10, unique=True)


    def __unicode__(self):
        return self.code


class Exam(models.Model):
    name = models.CharField(max_length=80)
    topic = models.CharField(max_length=20, verbose_name="Tema del examen")
    date = models.DateField(verbose_name="fecha")
    begin_hour = models.TimeField(verbose_name="hora de inicio")
    end_hour = models.TimeField(verbose_name="hora de fin")
    number_question = models.IntegerField(verbose_name="numero de preguntas")
    teacher = models.ForeignKey(User, verbose_name="profesor", related_name='exam')
    students = models.ManyToManyField(User, verbose_name="estudiantes", related_name='exams', through='StudentExam')

    def __unicode__(self):
        return self.name


class StudentExam(models.Model):
    student = models.ForeignKey(User, verbose_name="alumno", related_name='studentexam')
    exam = models.ForeignKey('Exam', verbose_name="examen", related_name='studentexam')
    record = models.PositiveIntegerField(max_length=2, blank=True, default=0)
    is_resolved = models.BooleanField(default=False, blank=True)

    def __unicode__(self):
        return u"{}:{}".format(self.student.username, self.exam.name)


class Question(models.Model):
    question_text = models.CharField(max_length=80)
    topic = models.CharField(max_length=20)
    exams = models.ManyToManyField('Exam', verbose_name="preguntas", blank=True, related_name='questions')

    def __unicode__(self):
        return self.question_text


class Choice(models.Model):
    choice_text = models.CharField(max_length=65)
    question = models.ForeignKey('Question', verbose_name="pregunta relacionada", related_name='choices')
    is_answer = models.BooleanField(default=False)

    def __unicode__(self):
        return self.choice_text


class QuestionStudent(models.Model):
    question = models.ForeignKey('Question', verbose_name="pregunta del estudiante", related_name='questionsstudent')
    student = models.ForeignKey(User, verbose_name="estudiante que responde a la pregunta",
                                related_name='questionsstudent')
    is_correct = models.BooleanField(default=False)


    def __unicode__(self):
        return unicode(self.pk)


class QuestionStudentChoice(models.Model):
    question_student = models.ForeignKey('QuestionStudent', verbose_name="pregunta a evaluar del estudiante",
                                         related_name='questionstudenchoices')
    choices = models.ForeignKey('Choice', verbose_name="opcion marcada por el estudiante",
                                related_name='questionstudenchoices')

    def __unicode__(self):
        return unicode(self.pk)
