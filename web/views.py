from braces.views import AnonymousRequiredMixin, LoginRequiredMixin
from class_based_auth_views.views import LoginView
from django.contrib.auth.forms import AuthenticationForm
from django.core.urlresolvers import reverse_lazy
from django.http import HttpResponseRedirect
from django.http.response import Http404
from django.views.generic import View, ListView, CreateView, DetailView
from web.models import Student, Teacher, Exam, Question, Choice, QuestionStudent, QuestionStudentChoice, StudentExam
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from django.utils import timezone


class MyLoginView(AnonymousRequiredMixin, LoginView):
    template_name = "login.html"
    authenticated_redirect_url = reverse_lazy('page_principal')
    form_class = AuthenticationForm


class PortalView(LoginRequiredMixin, View):
    def get(self, request, *args, **kwargs):
        if Student.objects.filter(user=request.user).exists():
            return HttpResponseRedirect('/portal/student/')
        elif Teacher.objects.filter(user=request.user).exists():
            return HttpResponseRedirect('/portal/teacher/')
            #return render(request, 'page_teacher.html')
        else:
            raise Http404


class PortalTeacherView(ListView):
    authentication_classes = SessionAuthentication,
    permission_classes = IsAuthenticated,
    template_name = "page_teacher.html"
    context_object_name = 'examenes'

    def get_queryset(self):
        return Exam.objects.filter(teacher=self.request.user)

    def get_context_data(self, **kwargs):
        context = super(PortalTeacherView, self).get_context_data(**kwargs)
        context['examen_resuelto'] = StudentExam.objects.filter(student=self.request.user)
        return context


class CrearExamenTeacher(LoginRequiredMixin, CreateView):
    model = Exam
    template_name = "crear_examen_profe.html"
    success_url = reverse_lazy('page_teacher')
    fields = ['name', 'topic', 'date', 'begin_hour', 'end_hour', 'number_question']
    authentication_classes = SessionAuthentication,
    permission_classes = IsAuthenticated,


    def get_context_data(self, **kwargs):
        context = super(CrearExamenTeacher, self).get_context_data(**kwargs)
        context['estudiantes'] = Student.objects.all()

        return context


    def form_valid(self, form):
        examen = form.instance
        #form.instance.teacher = Teacher.objects.get(user=self.request.user)
        form.instance.teacher = self.request.user
        preguntas = Question.objects.filter(topic=form.cleaned_data.get('topic')).order_by('?')[
                    0:form.cleaned_data.get('number_question')]
        val = super(CrearExamenTeacher, self).form_valid(form)
        examen.questions.add(*list(preguntas))
        lista = self.request.POST.get('list_select')
        for llave in self.request.POST.keys():
            if "estudiante-" in llave:
                pass
                usuario = Student.objects.get(code=self.request.POST[llave]).user
                usuario.save()
                usuario.exams.add(examen)
                StudentExam.student = usuario
                StudentExam.exam = examen
                StudentExam.save()
        return val


class DetailExamView(DetailView):
    model = Exam
    template_name = "detail_exam.html"
    context_object_name = 'examen'


class PortalStudentView(ListView):
    authentication_classes = SessionAuthentication,
    permission_classes = IsAuthenticated,
    template_name = "page_student.html"
    context_object_name = 'examenes'

    def get_queryset(self):
        return Exam.objects.filter(students=self.request.user)


class ExamStudentView(DetailView):
    model = Exam
    template_name = "list_question.html"
    authentication_classes = SessionAuthentication,
    permission_classes = IsAuthenticated,
    context_object_name = 'examen'

    def get_object(self, queryset=None):
        now = timezone.now().astimezone(timezone.get_default_timezone()).time()
        now_date = timezone.now().astimezone(timezone.get_default_timezone()).date()
        examen_mostrar = Exam.objects.get(pk=self.kwargs['pk'])
        if (examen_mostrar.date == now_date) and (examen_mostrar.begin_hour <= now) and (
                    examen_mostrar.end_hour >= now):
            return examen_mostrar
        else:
            return Exam.objects.none()

    def post(self, request, *args, **kwargs):
        for llave in self.request.POST.keys():
            if 'choice-' in llave:
                pass
                opcion = Choice.objects.get(pk=self.request.POST[llave])
                obj, create = QuestionStudent.objects.get_or_create(question=opcion.question, student=self.request.user)
                QuestionStudentChoice.objects.create(
                    question_student=QuestionStudent.objects.get(question=opcion.question), choices=opcion)

                # if not opcion.is_answer:
                #     obj.is_correct = False
                #     obj.save()

        for pregunta in Exam.objects.get(pk=self.kwargs['pk']).questions.all():
            correctas = set(pregunta.choices.values_list('id', flat=True).filter(is_answer=True))
            incorrectas = set(pregunta.choices.values_list('id', flat=True).filter(is_answer=False))
            mias = set([int(id_choice[7:]) for id_choice in self.request.POST.keys() if "choice-" in id_choice])
            if correctas.issubset(mias) and incorrectas.isdisjoint(mias):
                qs = QuestionStudent.objects.get(question=pregunta, student=self.request.user)
                qs.is_correct = True
                qs.save()

        se = StudentExam.objects.get(exam=Exam.objects.get(pk=self.kwargs['pk']), student=self.request.user)
        se.record = len(QuestionStudent.objects.filter(is_correct=True))
        se.is_resolved = True
        se.save()

        return HttpResponseRedirect('/portal/student/')