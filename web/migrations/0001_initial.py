# -*- coding: utf-8 -*-
from south.utils import datetime_utils as datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Teacher'
        db.create_table(u'web_teacher', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='teacher', unique=True, to=orm['auth.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=10)),
        ))
        db.send_create_signal(u'web', ['Teacher'])

        # Adding model 'Student'
        db.create_table(u'web_student', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('user', self.gf('django.db.models.fields.related.OneToOneField')(related_name='student', unique=True, to=orm['auth.User'])),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('code', self.gf('django.db.models.fields.CharField')(unique=True, max_length=10)),
        ))
        db.send_create_signal(u'web', ['Student'])

        # Adding model 'Exam'
        db.create_table(u'web_exam', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('topic', self.gf('django.db.models.fields.CharField')(max_length=20)),
            ('date', self.gf('django.db.models.fields.DateField')()),
            ('begin_hour', self.gf('django.db.models.fields.TimeField')()),
            ('end_hour', self.gf('django.db.models.fields.TimeField')()),
            ('number_question', self.gf('django.db.models.fields.IntegerField')()),
            ('teacher', self.gf('django.db.models.fields.related.ForeignKey')(related_name='exam', to=orm['auth.User'])),
        ))
        db.send_create_signal(u'web', ['Exam'])

        # Adding M2M table for field students on 'Exam'
        m2m_table_name = db.shorten_name(u'web_exam_students')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('exam', models.ForeignKey(orm[u'web.exam'], null=False)),
            ('user', models.ForeignKey(orm[u'auth.user'], null=False))
        ))
        db.create_unique(m2m_table_name, ['exam_id', 'user_id'])

        # Adding model 'Question'
        db.create_table(u'web_question', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question_text', self.gf('django.db.models.fields.CharField')(max_length=80)),
            ('topic', self.gf('django.db.models.fields.CharField')(max_length=20)),
        ))
        db.send_create_signal(u'web', ['Question'])

        # Adding M2M table for field exams on 'Question'
        m2m_table_name = db.shorten_name(u'web_question_exams')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('question', models.ForeignKey(orm[u'web.question'], null=False)),
            ('exam', models.ForeignKey(orm[u'web.exam'], null=False))
        ))
        db.create_unique(m2m_table_name, ['question_id', 'exam_id'])

        # Adding model 'Choice'
        db.create_table(u'web_choice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('choice_text', self.gf('django.db.models.fields.CharField')(max_length=65)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['web.Question'])),
            ('is_answer', self.gf('django.db.models.fields.BooleanField')(default=False)),
        ))
        db.send_create_signal(u'web', ['Choice'])

        # Adding model 'QuestionStudent'
        db.create_table(u'web_questionstudent', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['web.Question'])),
            ('student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['auth.User'])),
        ))
        db.send_create_signal(u'web', ['QuestionStudent'])

        # Adding model 'QuestionStudentChoice'
        db.create_table(u'web_questionstudentchoice', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('question_student', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['web.QuestionStudent'])),
            ('choices', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['web.Choice'])),
        ))
        db.send_create_signal(u'web', ['QuestionStudentChoice'])


    def backwards(self, orm):
        # Deleting model 'Teacher'
        db.delete_table(u'web_teacher')

        # Deleting model 'Student'
        db.delete_table(u'web_student')

        # Deleting model 'Exam'
        db.delete_table(u'web_exam')

        # Removing M2M table for field students on 'Exam'
        db.delete_table(db.shorten_name(u'web_exam_students'))

        # Deleting model 'Question'
        db.delete_table(u'web_question')

        # Removing M2M table for field exams on 'Question'
        db.delete_table(db.shorten_name(u'web_question_exams'))

        # Deleting model 'Choice'
        db.delete_table(u'web_choice')

        # Deleting model 'QuestionStudent'
        db.delete_table(u'web_questionstudent')

        # Deleting model 'QuestionStudentChoice'
        db.delete_table(u'web_questionstudentchoice')


    models = {
        u'auth.group': {
            'Meta': {'object_name': 'Group'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '80'}),
            'permissions': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['auth.Permission']", 'symmetrical': 'False', 'blank': 'True'})
        },
        u'auth.permission': {
            'Meta': {'ordering': "(u'content_type__app_label', u'content_type__model', u'codename')", 'unique_together': "((u'content_type', u'codename'),)", 'object_name': 'Permission'},
            'codename': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'content_type': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['contenttypes.ContentType']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '50'})
        },
        u'auth.user': {
            'Meta': {'object_name': 'User'},
            'date_joined': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'email': ('django.db.models.fields.EmailField', [], {'max_length': '75', 'blank': 'True'}),
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'groups': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Group']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'True'}),
            'is_staff': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'is_superuser': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_login': ('django.db.models.fields.DateTimeField', [], {'default': 'datetime.datetime.now'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'blank': 'True'}),
            'password': ('django.db.models.fields.CharField', [], {'max_length': '128'}),
            'user_permissions': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "u'user_set'", 'blank': 'True', 'to': u"orm['auth.Permission']"}),
            'username': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30'})
        },
        u'contenttypes.contenttype': {
            'Meta': {'ordering': "('name',)", 'unique_together': "(('app_label', 'model'),)", 'object_name': 'ContentType', 'db_table': "'django_content_type'"},
            'app_label': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'model': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'web.choice': {
            'Meta': {'object_name': 'Choice'},
            'choice_text': ('django.db.models.fields.CharField', [], {'max_length': '65'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_answer': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.Question']"})
        },
        u'web.exam': {
            'Meta': {'object_name': 'Exam'},
            'begin_hour': ('django.db.models.fields.TimeField', [], {}),
            'date': ('django.db.models.fields.DateField', [], {}),
            'end_hour': ('django.db.models.fields.TimeField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'number_question': ('django.db.models.fields.IntegerField', [], {}),
            'students': ('django.db.models.fields.related.ManyToManyField', [], {'related_name': "'exams'", 'symmetrical': 'False', 'to': u"orm['auth.User']"}),
            'teacher': ('django.db.models.fields.related.ForeignKey', [], {'related_name': "'exam'", 'to': u"orm['auth.User']"}),
            'topic': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'web.question': {
            'Meta': {'object_name': 'Question'},
            'exams': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'related_name': "'questions'", 'blank': 'True', 'to': u"orm['web.Exam']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question_text': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'topic': ('django.db.models.fields.CharField', [], {'max_length': '20'})
        },
        u'web.questionstudent': {
            'Meta': {'object_name': 'QuestionStudent'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.Question']"}),
            'student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['auth.User']"})
        },
        u'web.questionstudentchoice': {
            'Meta': {'object_name': 'QuestionStudentChoice'},
            'choices': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.Choice']"}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'question_student': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['web.QuestionStudent']"})
        },
        u'web.student': {
            'Meta': {'object_name': 'Student'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'student'", 'unique': 'True', 'to': u"orm['auth.User']"})
        },
        u'web.teacher': {
            'Meta': {'object_name': 'Teacher'},
            'code': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '10'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '80'}),
            'user': ('django.db.models.fields.related.OneToOneField', [], {'related_name': "'teacher'", 'unique': 'True', 'to': u"orm['auth.User']"})
        }
    }

    complete_apps = ['web']