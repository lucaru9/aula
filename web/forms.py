from django import forms
from django.contrib.auth.models import User
from web.models import Student


class StudentForm(forms.ModelForm):
    username = forms.CharField()
    password = forms.CharField(widget=forms.PasswordInput())

    def clean_username(self):
        username = self.cleaned_data.get('username')
        if User.objects.filter(username=username).exists():
            raise forms.ValidationError("Este nombre de usuario ya esta registrado")
        return username

    class Meta:
        model = Student
        exclude = ("user",)



